/* eslint-disable */
// api/index.js
import cookie from '@/utils/cookie';
import socket from '@/io';

/* AUTHENTICATION */
const authenticateSocket = () => {
  return new Promise((resolve, reject) => {
    const params = { strategy: "jwt", accessToken: cookie() };
    socket.emit("authenticate", params, function (message, data) {
      if (message != null || message != undefined) {
        reject();
      } else {
        resolve();
      }
    });
  })
};

const removePayment = id => {
  return new Promise((resolve, reject) => {
    // Remove payment
    socket.emit('remove', '/api/paymentsDetails', id, (error, response) => {
      error ? reject(error) : resolve(response);
    });
  });
};

const updateUnitStatus = (newStatus) => {
  return new Promise((resolve, reject) => {
    socket.emit('patch', 'units', newStatus.unitId, { statusId: newStatus.statusId }, (error, message) => {
      if (error) {
        reject(error);
      } else {
        resolve(message);
      }
    });
  })
};

const updateUnitExecutive = (newStatus) => {
  return new Promise((resolve, reject) => {
    socket.emit('patch', 'units', newStatus.unitId, { userId: newStatus.userId }, (error, message) => {
      if (error) {
        reject(error);
      } else {
        resolve(message);
      }
    });
  })
};

const updateUnitCustomer = newCustomer => {
  return new Promise((resolve, reject) => {
    socket.emit('patch', 'units', newCustomer.unitId, { customerId: newCustomer.customerId }, (error, message) => {
      error ? reject(error) : resolve(message);
    });
  })
}

const fetchUpdatedUnit = (unit) => {
  console.log(unit);
  return new Promise((resolve, reject) => {
    socket.emit('find', 'units', { id: unit }, (error, message) => {
      if (error) {
        reject(error);
      } else {
        resolve(message);
      }
    });
  })
};

const removeExecutiveFromUnit = data => {
  return new Promise((resolve, reject) => {
    socket.emit('patch', 'units', data, { userId: null }, (error, response) => {
      error ? reject(error) : resolve(response);
    });
  })
};

const removeCustomerFromUnit = data => {
  return new Promise((resolve, reject) => {
    socket.emit('patch', 'units', data, { customerId: null }, (error, response) => {
      error ? reject(error) : resolve(response);
    });
  })
};

/* USERS */

const patchPass = info => {
  return new Promise((resolve, reject) => {
    socket.emit('find', 'customers', { id: info.id }, (error, user) => {
      if (error) {
        console.log(error)
        reject(error);
      } else {
        console.log(user.data[0].password);
        console.log(info.password)
      }
    });
  });
};

const createUser = (user) => {
  return new Promise((resolve, reject) => {
    socket.emit('create', 'users', user, (error, response) => {
      if (error) {
        reject(error);
      } else {
        resolve(response);
      }
    });
  });
};
const currentUser = () => {
  return new Promise((resolve, reject) => {
    socket.emit('find', 'users', { current: 'true' }, (error, user) => {
      if (error) {
        reject(error);
      } else {
        resolve(user)
      }
    });
  });
};
const fetchUsers = ($skip) => {
  return new Promise((resolve, reject) => {
    socket.emit('find', 'users', { $skip }, (error, user) => {
      if (error) {
        reject(error);
      } else {
        resolve(user);
      }
    });
  })
};

const fetchCustomers = ($skip, query) => {
  query = query != null ? query : {};
  query['$skip'] = $skip;
  return new Promise((resolve, reject) => {
    socket.emit('find', 'customers', query, (error, customers) => {
      if (error) {
        reject(error);
      } else {
        resolve(customers);
      }
    });
  });
};

const fetchParkingLots = () => {
  return new Promise((resolve, reject) => {
    socket.emit('find', '/api/parkingDetails', { $sort: { id: 1 } }, (error, parking) => {
      if (error) {
        reject(error);
      } else {
        resolve(parking);
      }
    });
  });
}

const fetchQuotingInfo = unitId => {
  return new Promise((resolve, reject) => {
    socket.emit('find', '/roi', { unitId: unitId }, (error, quote) => {
      if (error) {
        reject(error);
      } else {
        resolve(quote);
      }
    });
  });
}

const patchParking = data => {
  if (data.currentUnit) {
    return new Promise((resolve, reject) => {
      socket.emit('patch', '/api/parkingDetails', data.id, { unitId: data.currentUnit, available: data.available }, (error, parking) => {
        if (error) {
          reject(error);
        } else {
          resolve(parking);
        }
      });
    });
  } else {
    return new Promise((resolve, reject) => {
      socket.emit('patch', '/api/parkingDetails', data.id, { unitId: null, available: data.available }, (error, parking) => {
        if (error) {
          reject(error);
        } else {
          resolve(parking);
        }
      });
    });
  }

}

const fetchUnitParking = unitId => {
  return new Promise((resolve, reject) => {
    socket.emit('find', '/api/parkingDetails', { unitId: unitId }, (error, parking) => {
      if (error) {
        reject(error);
      } else {
        resolve(parking);
      }
    });
  });
}

const fetchAllLeads = ($skip, query) => {
  query = query != null ? query : {};
  query['$skip'] = $skip;
  return new Promise((resolve, reject) => {
    socket.emit('find', 'leads', query, (error, leads) => {
      if (error) {
        console.log('error', error);
        reject(error);
      } else {
        resolve(leads);
      }
    });
  });
};

const fetchExecutives = () => {
  return new Promise((resolve, reject) => {
    socket.emit('find', 'users', (error, data) => {
      error ? reject(error) : resolve(data);
    });
  });
};

const fetchAgencies = () => {
  return new Promise((resolve, reject) => {
    socket.emit('find', 'users', { type: { $in: ['A', 'B', 'E'] } }, (error, data) => {
      error ? reject(error) : resolve(data);
    });
  });
};

const fetchAdvisers = () => {
  return new Promise((resolve, reject) => {
    socket.emit('find', 'users', { type: { $in: ['A', 'B', 'E', 'C', 'D', 'F', 'G'] } }, (error, data) => {
      error ? reject(error) : resolve(data);
    });
  });
};

const fetchReferences = () => {
  return new Promise((resolve, reject) => {
    socket.emit('find', 'users', { type: { $in: ['R'] } }, (error, data) => {
      error ? reject(error) : resolve(data);
    });
  });
};

const fetchUnitAgenciesInfo = unitId => {
  return new Promise((resolve, reject) => {
    socket.emit("api/commissions::find", { unitId: unitId }, (err, units) => {
      if (err) {
        reject(err);
      } else {
        resolve(units);
      }
    });
  });
}

const createNewCustomer = (customer) => {
  return new Promise((resolve, reject) => {
    socket.emit('create', 'customers', customer, (error, response) => {
      error ? reject(error) : resolve(response);
    });
  });
};

const deleteUser = (user) => {
  console.log(user.id)
  return new Promise((resolve, reject) => {
    socket.emit('remove', 'users', user.id, user, (error, response) => {
      if (error) {
        reject(error);
      } else {
        resolve(response);
      }
    });
  });
};

const patchUser = user => {
  return new Promise((resolve, reject) => {
    socket.emit('patch', 'users', user.id, user, (error, response) => {
      error ? reject(error) : resolve(response);
    });
  });
};

const patchCustomer = customer => {
  return new Promise((resolve, reject) => {
    socket.emit('patch', 'customers', customer.id, { password: customer.password }, (error, response) => {
      if (error) {
        console.log(error)
        reject(error)
      }
      resolve(response);
    });
  });
};

const fetchStatus = () => {
  return new Promise((resolve, reject) => {
    socket.emit('find', 'api/status', {}, (error, status) => {
      if (error) {
        reject(error);
      } else {
        resolve(status);
      }
    });
  });
};

const fetchClusters = () => {
  return new Promise((resolve, reject) => {
    socket.emit('find', 'api/clusters', { $paginate: 'false' }, (error, clusters) => {
      if (error) {
        reject(error);
      } else {
        //1 = "DISPONIBLE" 2 = "VENDIDO" 3 = "APARTADO"; 4 = "BLOQUEADO";
        let clustersByName = clusters.map(c => {
          return {
            id:c.id,
            name:c.name,
            abb:c.abb,
            status: {
              1:0,
              2:0,
              3:0,
              4:0,
              total:0
            }
          }
        })

        clustersByName.forEach((cluster, index) => {
          socket.emit('find', '/units', {clusterId: cluster.id}, (error, data) => {
            if (error) {
              reject(error);
            } else {
              data.data.forEach(dep => {
                cluster.status.total++
                cluster.status[dep.status.id]++
              })
            }
          });
        })

        resolve(clustersByName);
      }
    });
  });
};

const fetchUpdates = () => {
  return new Promise((resolve, reject) => {
    socket.emit('find', 'updates', (error, updates) => {
      if (error) {
        reject(error);
      } else {
        resolve(updates);
      }
    });
  });
};

const fetchClusterProgress = id => {
  return new Promise((resolve, reject) => {
    socket.emit('find', 'updates', { clusterId: id, $sort: { id: 1 } }, (error, updates) => {
      if (error) {
        reject(error);
      } else {
        resolve(updates);
      }
    });
  });
}

const fetchCountByCluster = clusterObj => {
  return new Promise((resolve, reject) => {
    socket.emit('find', '/api/countByCluster', clusterObj, (error, cluster) => {
      error ? reject(error) : resolve(cluster);
    });
  });
};

const fetchUnitsByCluster = ($skip, info) => {
  // Returns all clusters if user is not executive else returns only units assigned to said
  if (info) {

    return new Promise((resolve) => {
      socket.emit("api/departments::find", { clusterId: info.tower }, (error, lots) => {
        resolve(lots);
      });
    })

  }

};

const fetchCountDepartments = () => {
  return new Promise((resolve, reject) => {
    socket.emit('find', 'api/countByCluster', {}, (error, count) => {
      if (error) {
        reject(error);
      } else {
        resolve(count);
        //console.log(count.data);
      }
    });
  });
};

const getAttachments = unitId => {
  return new Promise((resolve, reject) => {
    socket.emit('find', 'attachments', { unitId: unitId }, (error, response) => {
      error ? reject(error) : resolve(response);
    });
  });
};

const deleteAttachment = data => {
  return new Promise((resolve, reject) => {
    socket.emit('remove', 'attachments', data, (error, response) => {
      error ? reject(error) : resolve(response);
    });
  });
};

const getAttachmentsCustomer = customerId => {
  return new Promise((resolve, reject) => {
    socket.emit('find', 'attachments', { customerId: customerId }, (error, response) => {
      error ? reject(error) : resolve(response);
    });
  });
};

const deleteAllAttachments = attachments => {
  return new Promise((resolve, reject) => {
    if (attachments.data) {
      for (let i = 0; i < attachments.data.length; i++) {
        socket.emit('remove', 'attachments', attachments.data[i].id, (error, payments) => {
          error ? reject(error) : resolve(payments);
        });
      }
    }


  });
};

const updateCustomerInfo = customer => {
  return new Promise((resolve, reject) => {
    socket.emit('patch', 'customers', customer.id, customer, (error, response) => {
      // error ? reject(error) : resolve(response);
      if (error) {
        reject(error)
      }
      resolve(response);
    });
  });
};

const updateLeadInfo = lead => {
  return new Promise((resolve, reject) => {
    socket.emit('patch', 'leads', lead.id, lead, (error, response) => {
      error ? reject(error) : resolve(response);
    });
  });
};

const createNewLead = lead => {
  return new Promise((resolve, reject) => {
    socket.emit('create', 'leads', lead, (error, response) => {
      if (error) {
        reject(error)
      } else {
        resolve(response);
      }
    });
  });
};

const createNewAttachment = (attachment) => {
  return new Promise((resolve, reject) => {
    socket.emit('create', 'attachments', attachment, (error, response) => {
      error ? reject(error) : resolve(response);
    });
  });
};

/* Attachments */
const getS3Signature = file => {
  return new Promise((resolve, reject) => {
    socket.emit('create', 'attachments', file, (error, response) => {
      error ? reject(error) : resolve(response);
    });
  });
};

const generatePropesctoGuide = type => {
  return new Promise((resolve, reject) => {
    socket.emit('create', 'attachments', type, (err, res) => {
      err ? reject(err) : resolve(res);
    });
  });
};

/* Units */
const getUnits = () => {
  return new Promise((resolve, reject) => {
    socket.emit('find', '/units', {}, (error, count) => {
      if (error) {
        reject(error);
      } else {
        resolve(count);
      }
    });
  });
};

const updateUnitInfo = data => {
  return new Promise((resolve, reject) => {
    socket.emit('patch', '/units', data.id, data, (error, res) => {
      // error ? reject(error) : resolve(res);
      if (error) {
        console.log(error);
        reject(error);
      } else {
        resolve(res);
      }
    });
  });
};

/* Units by User */
const getUnitsByClient = userId => {
  return new Promise((resolve, reject) => {

    // Check if userId exists
    if (userId === null || userId === undefined) {
      reject('Not valid user info.');
    }

    // Fetch all units that belongs to this userId
    socket.emit('find', '/units', { customerId: userId }, (err, units) => {
      // Validate if current call has valid data
      if (units.total > 0) {
        resolve(units.data);
      } else {
        reject(err);
      }
    });
  });
};

const getUnitsByStage = (cluster) => {
  return new Promise((resolve, reject) => {
    socket.emit('find', '/units', { clusterId: cluster }, (error, data) => {
      error ? reject(error) : resolve(data);
    });
  });
};

const removeCustomer = customerId => {
  return new Promise((resolve, reject) => {
    socket.emit('remove', 'customers', customerId, (error, response) => {
      error ? reject(error) : resolve(response);
    });
  });
};

const removeLead = leadId => {
  return new Promise((resolve, reject) => {
    socket.emit('remove', 'leads', leadId, (error, response) => {
      error ? reject(error) : resolve(response);
    });
  });
};

const removeUpdate = id => {
  return new Promise((resolve, reject) => {
    socket.emit('remove', 'updates', id, (error, response) => {
      if (error) {
        reject(error)
      }
      resolve(resolve)
    });
  });
}

const getUnitsInfo = () => {
  return new Promise((resolve, reject) => {
    socket.emit('find', '/units', (error, data) => {
      if (error) {
        reject(error);
      } else {
        const departments = data.data;
        resolve(departments);
      }
    });
  });
};

const fetchPaymentsByUnit = unitId => {
  return new Promise((resolve, reject) => {
    // First we get paymentId in order
    // to get all paymentsDetails related
    // to this paymentId
    socket.emit('find', '/api/payments', { unitId: unitId }, (error, payment) => {
      // Validate if we don't get an error
      if (error) {
        reject(error);
      }

      // Validate if we have
      // valid data
      if (payment.total === 0) {
        resolve(null);
        return;
      }

      // Get paymentId
      const paymentId = payment.data[0].id;

      // Fetch all paymentDetails that belongs to
      // this paymentId
      socket.emit('find', '/api/paymentsDetails', { paymentId: paymentId, $sort: { paymentNo: 1 } }, (error, payments) => {
        error ? reject(error) : resolve(payments);
      });
    });
  });
};

/* Payments */
const createPayment = (date, payment) => {
  return new Promise((resolve, reject) => {
    socket.emit('create', '/api/payments', { date: date, totalToPay: payment.totalToPay, unitId: payment.unitId }, (error, response) => {
      error ? reject(error) : resolve(response);
    });
  });
};

const updatePaymentDetails = payment => {
  return new Promise((resolve, reject) => {
    socket.emit('patch', '/api/paymentsDetails', payment.id, payment, (error, data) => {
      error ? reject(error) : resolve(data);
    });
  });
};

const addPayment = paymentDetail => {
  return new Promise((resolve, reject) => {

    // Check paymentNo
    if (paymentDetail.paymentNo === 1) {
      // Payment Object
      const payment = {
        totalPayments: 1,
        totalSale: null,
        totalToPay: null,
        differencePayments: null,
        unitId: paymentDetail.unitId
      }

      socket.emit('create', '/api/payments', payment, (error, response) => {
        if (error) {
          reject(error);
        }

        const paymentMaster = response;
        paymentDetail.paymentId = paymentMaster.id;

        // Set Payment Detail
        socket.emit('create', '/api/paymentsDetails', paymentDetail, (error, response) => {
          error ? reject(error) : resolve(response);
        });

      });
    } else {
      // If payments already exists
      socket.emit('create', '/api/paymentsDetails', paymentDetail, (error, response) => {
        error ? reject(error) : resolve(response);
      });
    }

  });
};

/* CONTRACTS */

const createContract = (contractUnit) => {
  return new Promise((resolve, reject) => {
    socket.emit('api/contracts::find', { unitId: contractUnit.unitId }, (err, contract) => {

      // If there's a contract with that unitId it updates the info, else it creates a new one
      if (contract.data.length > 0) {
        // Patch a contact
        socket.emit('patch', 'api/contracts', contractUnit.id, { paymentPlan: contractUnit.paymentPlan }, (error, data) => {
          error ? reject(error) : resolve(data);
        });
      } else {
        // Creates contract
        socket.emit('api/contracts::create', contractUnit
          , (error, response) => {
            error ? reject(error) : resolve(response);
          });
      }
    });
  });
};

const erasePaymentPlan = contractUnit => {
  return new Promise((resolve, reject) => {
    socket.emit('patch', 'api/contracts', contractUnit.id, { paymentPlan: null }, (error, response) => {
      error ? reject(error) : resolve(response);
    });
  });
}


const deletePaymentDetails = payments => {
  return new Promise((resolve, reject) => {
    // Payments : Array[]
    // Before deleting all payments details
    // we retrieve the current paymentId
    if (payments) {

      const paymentId = payments[0].paymentId;

      // So now we're gonna delete all payment details
      // by using their Id
      for (let i = 0; i < payments.length; i++) {
        socket.emit('remove', '/api/paymentsDetails', payments[i].id, (error, payments) => {
          if (error) { reject(error); }
        });
      }

      // After deleting all payment details now
      // we have to delete the payment related
      if (paymentId !== null || paymentId !== undefined) {
        socket.emit('remove', '/api/payments', paymentId, (error, payment) => {
          error ? reject(error) : resolve(payment);
        });
      } else {
        reject(new Error('Error: Payment not defined'));
      }

    }

  });
};

const fetchDepartmentsByCluster = ($skip, info) => {
  // Returns all clusters if user is not executive else returns only units assigned to said
  if (info) {
    if (info.type == 'B') {
      return new Promise((resolve, reject) => {
        socket.emit('api/departments::find', { userId: info.user }, (err, contract) => {
          const contractsByExecutive = [];
          if (contract) {
            contract.data.forEach(function (u) {
              if (u.clusterId === info.tower) {
                console.log("MATCH");
                console.log(u);
                contractsByExecutive.push(u);
              }
            })
          }
          resolve(contractsByExecutive);
        });
      })
    } else {

      return new Promise((resolve) => {
        socket.emit("api/departments::find", { clusterId: info.tower }, (error, lots) => {
          resolve(lots);
        });
      })
    }
  }
}

const fetchUnitContract = (unitId) => {
  return new Promise((resolve, reject) => {
    socket.emit("api/contracts::find", { unitId: unitId }, (error, response) => {
      if (error) {
        reject(error)
      } else {
        resolve(response.data[0]);
      }
      // error ? reject(error) : resolve(response);
    });
  });
}

const fetchCommissions = ($skip, query, unitId) => {
  query = query != null ? query : {};
  query['$skip'] = $skip;
  return new Promise((resolve) => {
    socket.emit("api/commissions::find", { unitId: unitId }, (error, lots) => {
      if (error) {
        reject(error);
      }
      resolve(lots);
    });
  })
};

const createCommissionByUnit = (commission) => {
  return new Promise((resolve, reject) => {
    socket.emit('create', '/api/commissions', commission, (error, response) => {
      error ? reject(error) : resolve(response);
    });
  });
};

const removeCommission = id => {
  return new Promise((resolve, reject) => {
    socket.emit('remove', '/api/commissions', id, (error, response) => {
      error ? reject(error) : resolve(response);
    });
  });
};

const updateCommissionByUnit = commission => {
  return new Promise((resolve, reject) => {
    socket.emit('patch', '/api/commissions', commission.id, commission, (error, data) => {
      error ? reject(error) : resolve(data);
    });
  });
};

const createSalesContract = contract => {
  return new Promise((resolve, reject) => {
    socket.emit('create', '/attachments', { contract: contract, type: 'Contrato De Venta' }, (err, res) => {
      err ? reject(err) : resolve(res);
    });
  });
};

const generateNewAttachment = contract => {
  return new Promise((resolve, reject) => {
    socket.emit('create', '/attachments', { contract }, (err, res) => {
      err ? reject(err) : resolve(res);
    });
  });
};

const getUnitsByExec = userData => {
  return new Promise((resolve, reject) => {
    socket.emit('find', '/units', { userId: userData }, (err, units) => {
      if (err) {
        reject(err);
      } else {
        resolve(units);
      }
    });
  });
}

const fetchDepartments = ($skip, query) => {
  query = query != null ? query : {};
  query['$skip'] = $skip;
  return new Promise((resolve) => {
    socket.emit('find', '/units', query, (err, lots) => {
      if (err)
        reject(err)
      resolve(lots);
    });
  })
};

const getS3SignatureUpdate = file => {
  return new Promise((resolve, reject) => {
    socket.emit('create', 'updates', file, (error, response) => {
      if (error) {
        console.log(error);
      } else {
        resolve(response);
      }
      //  error ? reject(error) : resolve(response);
    });
  });
};

export {
  authenticateSocket,
  fetchStatus,
  fetchExecutives,
  fetchAgencies,
  fetchClusters,
  fetchCountByCluster,
  fetchUnitsByCluster,
  updateUnitStatus,
  createUser,
  currentUser,
  deleteUser,
  fetchUsers,
  patchUser,
  removeCustomer,
  removeLead,
  createNewCustomer,
  fetchCustomers,
  getS3Signature,
  getUnits,
  getUnitsByStage,
  getUnitsInfo,
  updateUnitExecutive,
  updateUnitCustomer,
  fetchUpdatedUnit,
  removeExecutiveFromUnit,
  removeCustomerFromUnit,
  getAttachments,
  deleteAttachment,
  deleteAllAttachments,
  getAttachmentsCustomer,
  createNewAttachment,
  updateCustomerInfo,
  updateLeadInfo,
  generatePropesctoGuide,
  fetchPaymentsByUnit,
  createPayment,
  updatePaymentDetails,
  addPayment,
  removePayment,
  createContract,
  fetchDepartmentsByCluster,
  fetchUnitContract,
  updateUnitInfo,
  deletePaymentDetails,
  fetchCommissions,
  createCommissionByUnit,
  removeCommission,
  updateCommissionByUnit,
  erasePaymentPlan,
  fetchAllLeads,
  createNewLead,
  createSalesContract,
  generateNewAttachment,
  getUnitsByClient,
  getUnitsByExec,
  fetchDepartments,
  getS3SignatureUpdate,
  fetchUpdates,
  removeUpdate,
  fetchClusterProgress,
  patchCustomer,
  fetchParkingLots,
  fetchQuotingInfo,
  patchParking,
  fetchUnitParking,
  fetchReferences,
  fetchUnitAgenciesInfo,
  fetchAdvisers
}
