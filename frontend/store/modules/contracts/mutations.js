
const CONTRACTS_UPDATED = (state, contracts) => {
    state.contracts = contracts;
  };
  
  const ROI_UPDATED = (state, payload) => {
    state.allROI = payload;
  }
  
  const CONTRACTS_BY_PAID_REF = (state, payload) => {
    state.contractsByPaidRef = payload;
  };
  
  const PAGINATION_UPDATED = (state, pagination) => {
    state.pagination = pagination;
  };
  const GO_NEXT_PAGE = (state) => {
    var pagination = state.pagination;
    pagination.index++;
    pagination.skip = pagination.index * pagination.limit;
    state.pagination = pagination;
  };
  const GO_PREV_PAGE = (state) => {
    var pagination = state.pagination;
    pagination.index--;
    pagination.skip = pagination.index * pagination.limit;
    state.pagination = pagination;
  };
  
  const SET_VIEW_TYPE = (state) => {
     state.isList = !state.isList;
  };
  
  const SET_FILTER = (state, payload) => {
  
    if(payload != state.filterValue) {
      state.filterValue = payload;
    } else {
      state.filterValue = 'id'
    }
  
  };
  
  const REMOVE_FILTER = (state, payload) => {
  
    state.specialFilter.forEach (filter => {
      if(filter.value != null) {
        filter.value = null
      }
    })
  
  };
  
  const SPECIAL_FILTER = (state, payload) => {
    state.specialFilter.forEach (filter => {
      if (payload.id === filter.id) {
        if (payload.value !== filter.value) {
          filter.value = payload.value
        } else {
          filter.value = null
        }
      }
    })
  };
  
  // const DEPARTMENT_UPDATED = (state, department) => {
  //   // console.log('updating register...', JSON.stringify(house));
  //   var filteredDepartment = state.departments.filter(h => h.id == department.id);
  //   // console.log(filteredHouse.length);
  //   if (filteredDepartment.length > 0) {
  //     for (let i = 0; i < state.departments.length; i++) {
  //       let h = state.departments[i];
  //       if (h.id == department.id) {
  //         for (var key in department) {
  //           h[key] = department[key];
  //         }
  //       }
  //     }
  //   }
  // };
  
  
  const QUERY_UPDATED = (state, query) => {
    state.query = query;
  }
  
  const NEW_CONTRACT = (state,payload)=> {
    state.selectedDep = payload;
  }
  
  const CONTRACTS_BY_USER = (state, payload) => {
    state.clientUnits = payload;
  }
  
  const CONTRACT_UNIT = (state,payload) => {
    state.unitContract = payload;
  }
  
  export default {
    CONTRACTS_UPDATED,
    CONTRACTS_BY_PAID_REF,
    PAGINATION_UPDATED,
    GO_NEXT_PAGE,
    GO_PREV_PAGE,
    CONTRACT_UNIT,
    ROI_UPDATED,
    // DEPARTMENT_UPDATED,
    QUERY_UPDATED,
    SET_VIEW_TYPE,
    SET_FILTER,
    SPECIAL_FILTER,
    REMOVE_FILTER,
    NEW_CONTRACT,
    CONTRACTS_BY_USER
  };
  