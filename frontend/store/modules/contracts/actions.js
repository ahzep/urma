import { 
  // fetchContracts,
  createContract,
  erasePaymentPlan,
  // fetchContractsByPaidRef,
  // getFirstDraftContract,
  // updateContractInfo,
  updateCustomerInfo,
  // getContractByClient,
  createFirstDraft,
  fetchUnitContract,
  // createLeaseContract,
  // fetchROI
  } from '@/api';
import socket from '@/io';


// const getContracts = (context) => {
//   // console.log('fetching houses...');
//   fetchContracts(context.state.pagination.skip, context.state.query).then(response => {
//     if(response.data !== undefined) {
//       //call context.commit
//       context.commit('CONTRACTS_UPDATED', response.data);

//       let pagination = {
//         total: response.total,
//         limit: response.limit,
//         skip: response.skip,
//         pages: response.total / response.limit,
//         index: Math.floor(response.skip / response.limit),
//       };

//       context.commit('PAGINATION_UPDATED', pagination);
//     }

//   }).catch(error => {
//     console.log(error);
//   });
// };

// const getContractsByPaidRef = context => {
//   return new Promise((resolve, reject) => {
//     fetchContractsByPaidRef().then(response => {
//       context.commit('CONTRACTS_BY_PAID_REF', response);
//       resolve(response);
//     }).catch(err => {
//       reject(err);
//     });
//   });
// };

const getUnitContract = (context, unitId) => {
  return new Promise((resolve, reject) => {
    fetchUnitContract(unitId).then(response => {
      context.commit('CONTRACT_UNIT', response);
      resolve(response);
    }).catch(err => {
      reject(err);
    });
  })
};

// const editContractInfo = (context, data) => {
//   return new Promise((resolve, reject) => {
//     updateCustomerInfo(data.customer).then(() => {
//       updateContractInfo(data).then(response => {
//         resolve(response);
//       }).catch(err => {
//         reject(err);
//       });
//     }).catch(error => {
//       reject(error);
//     });
//   });
// };

const newContract = (context, contract) => {
  return new Promise((resolve, reject) => {
    createContract(contract).then(response => {
      resolve(response);
    }).catch(err => {
      reject(err);
    });
  });
};

// const newLeaseContract = (context, contract) => {
//   return new Promise((resolve, reject) => {
//     createLeaseContract(contract).then(response => {
//       resolve(response);
//     }).catch(err => {
//       reject(err);
//     });
//   });
// };

const nextPage = (context) => {

  const { index, pages } = context.state.pagination;
  if (index < pages - 1) {
    //add one to the index.
    context.commit('GO_NEXT_PAGE');
    //when this happens, you should retrieve the new page.
    getContracts(context);
  }

};

const prevPage = (context) => {
  const { index, pages } = context.state.pagination;
  if (index > 0) {
    //add one to the index.
    context.commit('GO_PREV_PAGE');
    //when this happens, you should retrieve the new page.
    getContracts(context);
  }
};

const goSearch = (context, query) => {
  context.commit('QUERY_UPDATED', query);
  getContracts(context);
}

const setFilter = (context, payload) => {

  context.commit('SET_FILTER', payload);

};

const setSpecialFilter = (context, payload) => {

  context.commit('SPECIAL_FILTER', payload);

};

const removeSpecialFilter = (context, payload) => {

  context.commit('REMOVE_FILTER', payload);

};

const listenEvents = ({ dispatch, commit }) => {
  //console.log('listen...');
  // socket.on('api/departments patched', function (message) {
  //   commit('DEPARTMENT_UPDATED', message);
  // });
  // socket.on('api/departments updated', function (message) {
  //   commit('DEPARTMENTS_UPDATED', message);
  // });
  //
  // socket.on('api/departments created', function (message) {
  //   dispatch('getDepartments');
  // });

}

const setListType = ( context, commit ) => {
  context.commit('SET_VIEW_TYPE');
}

// const getFirstDraftPDF = (context, method) => {
//   return new Promise((resolve, reject) => {
//     getFirstDraftContract(method).then(response => {
//       resolve(response);
//     }).catch(error => {
//       reject(error);
//     });
//   });
// };

// const fetchContractByUser = (context, userId) => {
//   return new Promise((resolve, reject) => {
//     getContractByClient(userId).then(contracts => {
//       context.commit('CONTRACTS_BY_USER', contracts);
//       resolve(contracts);
//     }).catch(err => {
//       reject(err);
//     });
//   });
// };

const getCustomer = (context, unitId) => {
// ?
}

// const sendFirstDraft = (context, contract) => {
//   return new Promise((resolve, reject) => {
//     createFirstDraft(contract).then(pdf => {
//       resolve(pdf);
//     }).catch(err => {
//       reject(err);
//     });
//   });
// };

// const allROI = (context) => {
//   fetchROI(context.state.pagination.skip, context.state.query).then(response => {
    
//     if(response.data !== undefined) {
//       //call context.commit
//       context.commit('ROI_UPDATED', response.data);
//     }

//   }).catch(error => {
//     console.log(error);
//   });
// };

const deletePaymentPlan = (context, contractId) => {
  return new Promise((resolve, reject) => {
    erasePaymentPlan(contractId).then(contracts => {
      resolve(contracts);
    }).catch(err => {
      reject(err);
    });
  });
};

export default {
  // getContracts,
  // getFirstDraftPDF,
  // getContractsByPaidRef,
  getUnitContract,
  listenEvents,
  newContract,
  // newLeaseContract,
  // editContractInfo,
  nextPage,
  prevPage,
  goSearch,
  setListType,
  setFilter,
  setSpecialFilter,
  removeSpecialFilter,
  // fetchContractByUser,
  getCustomer,
  // allROI,
  deletePaymentPlan
  // sendFirstDraft
}
