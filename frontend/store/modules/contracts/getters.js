const contracts = (state) => state.contracts;
const query = (state) => state.query;
const isListGetter = (state) => state.isList;
const filterValue = (state) => state.filterValue;
const specialSort = (state) => state.specialFilter;
const contractsByPaidRef = (state) => state.contractsByPaidRef;
const getContractsByClient = (state) => state.clientUnits;
const unitContract = (state) => state.unitContract;
const allROI = (state) => state.allROI;

export default {
  contracts,
  contractsByPaidRef,
  query,
  isListGetter,
  filterValue,
  specialSort,
  getContractsByClient,
  unitContract,
  allROI
}
