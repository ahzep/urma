const attachmentsByUnit = (state) => state.attachmentsByUnit;
const attachmentsByUser = (state) => state.attachmentsByUser;

export default {
    attachmentsByUnit,
    attachmentsByUser
}
