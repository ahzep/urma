const COMMISSIONS_UPDATED = (state, commissions) => {
  state.commissions = commissions;
};

const PAGINATION_UPDATED = (state, pagination) => {
  state.pagination = pagination;
};
const GO_NEXT_PAGE = (state) => {
  var pagination = state.pagination;
  pagination.index++;
  pagination.skip = pagination.index * pagination.limit;
  state.pagination = pagination;
};
const GO_PREV_PAGE = (state) => {
  var pagination = state.pagination;
  pagination.index--;
  pagination.skip = pagination.index * pagination.limit;
  state.pagination = pagination;
};

const SET_VIEW_TYPE = (state) => {
   state.isList = !state.isList;
};

const SET_FILTER = (state, payload) => {

  if(payload != state.filterValue) {
    state.filterValue = payload;
  } else {
    state.filterValue = 'id'
  }

};

const REMOVE_FILTER = (state, payload) => {

  state.specialFilter.forEach (filter => {
    if(filter.value != null) {
      filter.value = null
    }
  })

};

const SPECIAL_FILTER = (state, payload) => {
  state.specialFilter.forEach (filter => {
    if (payload.id === filter.id) {
      if (payload.value !== filter.value) {
        filter.value = payload.value
      } else {
        filter.value = null
      }
    }
  })
};

const SET_PAYMENTS_BY_UNIT = (state, payload) => {
  state.paymentsByUnit = payload;
};

const SET_COMMISSIONS_BY_UNIT = (state, payload) => {
  if(payload !== null)
    Vue.set(state, 'commissionByUnit', [...payload]);
  else
    state.commissionByUnit = null

};




const QUERY_UPDATED = (state, query) => {
  state.query = query;
}
export default {
  COMMISSIONS_UPDATED,
  PAGINATION_UPDATED,
  GO_NEXT_PAGE,
  GO_PREV_PAGE,
  SET_COMMISSIONS_BY_UNIT,
  QUERY_UPDATED,
  SET_VIEW_TYPE,
  SET_FILTER,
  SPECIAL_FILTER,
  REMOVE_FILTER,
  SET_PAYMENTS_BY_UNIT
};
