const commissions = (state) => state.commissions;
const query = (state) => state.query;
const isListGetter = (state) => state.isList;
const filterValue = (state) => state.filterValue;
const specialSort = (state) => state.specialFilter;
const paymentsByUnit = state => state.paymentsByUnit;
const commissionByUnit = state => state.commissions;

export default {
  commissions,
  query,
  isListGetter,
  filterValue,
  specialSort,
  paymentsByUnit,
  commissionByUnit
}
