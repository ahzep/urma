import { fetchCommissions, createPayment, addPayment, fetchPaymentsByUnit,
  // fetchCommissionsByUnit,
  removeCommission,createCommissionByUnit,updateCommissionByUnit, deletePaymentDetails, updatePaymentDetails, removePayment } from '@/api';
import socket from '@/io';


const getCommissions = (context, unitId) => {
  fetchCommissions(context.state.pagination.skip, context.state.query, unitId).then(response => {
    context.commit('COMMISSIONS_UPDATED', response.data);
    let pagination = {
      total: response.total,
      limit: response.limit,
      skip: response.skip,
      pages: response.total / response.limit,
      index: Math.floor(response.skip / response.limit),
    };
    context.commit('PAGINATION_UPDATED', pagination);
  }).catch(error => {
    console.log(error);
  });
};

const nextPage = (context) => {

  const { index, pages } = context.state.pagination;
  if (index < pages - 1) {
    //add one to the index.
    context.commit('GO_NEXT_PAGE');
    //when this happens, you should retrieve the new page.
    getCommissions(context);
  }

};
const prevPage = (context) => {
  const { index, pages } = context.state.pagination;
  if (index > 0) {
    //add one to the index.
    context.commit('GO_PREV_PAGE');
    //when this happens, you should retrieve the new page.
    getCommissions(context);
  }
};
const goSearch = (context, query) => {
  context.commit('QUERY_UPDATED', query);
  getCommissions(context);
}

const setFilter = (context, payload) => {

  context.commit('SET_FILTER', payload);

};

const setSpecialFilter = (context, payload) => {

  context.commit('SPECIAL_FILTER', payload);

};

const removeSpecialFilter = (context, payload) => {

  context.commit('REMOVE_FILTER', payload);

};

const listenEvents = ({ dispatch, commit }) => {
  //console.log('listen...');
  // socket.on('api/departments patched', function (message) {
  //   commit('DEPARTMENT_UPDATED', message);
  // });
  // socket.on('api/departments updated', function (message) {
  //   commit('DEPARTMENTS_UPDATED', message);
  // });
  //
  // socket.on('api/departments created', function (message) {
  //   dispatch('getDepartments');
  // });

}

const setListType = ( context, commit ) => {
  context.commit('SET_VIEW_TYPE');
}

const setPayments = (context, payment) => {
  return new Promise((resolve, reject) => {
    const today = new Date();
    createPayment(today, payment).then(response => {
      resolve(response);
    }).catch(error => {
      reject(error);
    });
  });
};

const setPayment = (context, payment) => {
  return new Promise((resolve, reject) => {
    addPayment(payment).then(response => {
      resolve(response);
    }).catch(error => {
      reject(error);
    });
  });
};

const deletePayment = (context, id) => {
  return new Promise((resolve, reject) => {
    removePayment(id).then(response => {
      resolve(response);
    }).catch(error => {
      reject(error);
    });
  });
};

const getPaymentsByUnit = (context, unitId) => {
  return new Promise((resolve, reject) => {
    fetchPaymentsByUnit(unitId).then(response => {
      if(response !== null) {
        context.commit('SET_PAYMENTS_BY_UNIT', response.data);
        resolve(response);
      } else {
        context.commit('SET_PAYMENTS_BY_UNIT', null);
      }
    }).catch(error => {
      reject(error);
    });
  });
};

// const getCommissionsByUnit = (context, unitId) => {
//   return new Promise((resolve, reject) => {
//     fetchCommissionsByUnit(unitId).then(response => {
//           console.log("fetching commission");
//           console.log(response);
//       if(response !== null) {
//         console.log("setting");
//         context.commit('SET_COMMISSIONS_BY_UNIT', response.data)

//       } else {
//         console.log('is null');
//         context.commit('SET_COMMISSIONS_BY_UNIT', []);

//       }
//         resolve(response);
//     }).catch(error => {
//       reject(error);
//     });
//   });
// };

const createCommission = (context, commission) => {
  return new Promise((resolve, reject) => {
    createCommissionByUnit(commission).then(response => {
      resolve(response);
    }).catch(error => {
      reject(error);
    });
  });
};

const updateCommision = (context, commision) => {
  return new Promise((resolve, reject) => {
    updateCommissionByUnit(commision).then(response => {
      resolve(response);
    }).catch(error => {
      reject(error);
    });
  });
};

const deleteCommission = (context, id) => {
  return new Promise((resolve, reject) => {
      removeCommission(id).then(response => {
        resolve(response);
      }).catch(error => {
        reject(error);
      });
  });
};

const deletePayments = (context, payments) => {
  return new Promise((resolve, reject) => {
    deletePaymentDetails(payments).then(response => {
      resolve(response);
    }).catch(error => {
      reject(error);
    })
  });
};

const updatePayment = (context, payment) => {
  return new Promise((resolve, reject) => {
    updatePaymentDetails(payment).then(response => {
      resolve(response);
    }).catch(error => {
      reject(error);
    });
  });
};


export default {
  getCommissions,
  listenEvents,
  // newDepartment,
  nextPage,
  prevPage,
  // updateDepartment,
  goSearch,
  setListType,
  setFilter,
  setSpecialFilter,
  removeSpecialFilter,
  setPayments,
  setPayment,
  getPaymentsByUnit,
  deletePayments,
  deletePayment,
  updatePayment,
  // getCommissionsByUnit,
  updateCommision,
  deleteCommission,
  createCommission
}
