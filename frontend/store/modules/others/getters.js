const status = (state) => state.status;
const clusters = (state) => state.clusters;
const countHouses = (state) => state.countHouses;
const countLots = (state) => state.countLots;
const countDepartments = (state) => state.countDepartments;
const customers = (state) => state.customers;
const getReferences = (state) => state.references;
const units = (state) => state.units;
const unitsByStage = (state) => state.unitsByStage;
const unitsInfo = (state) => state.unitsInfo;
const leads = (state) => state.leads;
const updates = (state) => state.updates.data;
const parking = (state) => state.parking;
const parkingsForUnit = (state) => state.parkingsUnit;
const quoteInfo = (state) => state.quoteInfo;

export default {
  clusters,
  status,
  countHouses,
  countLots,
  countDepartments,
  customers,
  getReferences,
  units,
  unitsByStage,
  unitsInfo,
  leads,
  updates,
  parking,
  parkingsForUnit,
  quoteInfo
};
