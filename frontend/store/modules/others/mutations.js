/* eslint-disable */
const IS_AUTHENTICATED = (state, isAuthenticated) => {
  state.isAuthenticated = isAuthenticated;
};

const ADD_BUTTON = (state, addButton) => {
  state.hasPlusButton = addButton;
};
const STATUS_UPDATED = (state, status) => {
  state.status = status;
};

const CLUSTERS_UPDATED = (state, clusters) => {
  state.clusters = clusters;
};

const COUNT_UNITS_UPDATED = (state, count) => {
  state.countUnits = count;
}
const CUSTOMERS_UPDATED = (state, customers) => {
  state.customers = customers;
}

const PARKING_UPDATED = (state, parking) => {
  state.parking = parking.data;
}

const QUOTING_UPDATED = (state, quote) => {
  state.quoteInfo = quote.data[0];
};

const UNITS_PARKING_UPDATED = (state, parking) => {
  state.parkingsUnit = parking.data;
};

const LEADS_UPDATED = (state, leads) => {
  state.leads = leads;
}

const UNITS = (state, units) => {
  state.units = units;
}

const UNITS_BY_STAGE = (state, stage) => {
  state.unitsByStage = stage;
}

const STAGES_INFO = (state, stage) => {
  state.unitsInfo = stage;
}

const UPDATES_UPDATED = (state, update) => {
  state.updates = update
}

export default {
  ADD_BUTTON,
  CLUSTERS_UPDATED,
  IS_AUTHENTICATED,
  STATUS_UPDATED,
  COUNT_UNITS_UPDATED,
  CUSTOMERS_UPDATED,
  LEADS_UPDATED,
  UNITS,
  UNITS_BY_STAGE,
  STAGES_INFO,
  UPDATES_UPDATED,
  PARKING_UPDATED,
  UNITS_PARKING_UPDATED,
  QUOTING_UPDATED
};
