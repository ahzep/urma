/* eslint-disable */
import {
  authenticateSocket,
  fetchStatus,
  fetchClusters,
  // fetchCountUnits,
  fetchUpdates,
  fetchCustomers,
  fetchUnitParking,
  fetchParkingLots,
  fetchQuotingInfo,
  patchParking,
  fetchAllLeads,
  createNewCustomer,
  getUnitsInfo,
  updateCustomerInfo,
  updateLeadInfo,
  removeCustomer,
  removeLead,
  createNewLead,
  removeUpdate,
  getS3SignatureUpdate,
  fetchClusterProgress
} from '@/api';

const authenticate = (context) => {
  return new Promise((resolve, reject) => {
    authenticateSocket().then(response => {
      context.commit('IS_AUTHENTICATED', true);
      resolve();
    }).catch(err => {
      reject();
    });
  });
};

const setPlusButton = (context, plusBtn) => {
  context.commit('ADD_BUTTON', plusBtn);
};

const setNewCustomer = (context, customer) => {
  return new Promise((resolve, reject) => {
    createNewCustomer(customer).then(res => {
      // context should be called to commit some mutation in order to send popups or something.
      resolve(res);
    }).catch(e => {
      reject(e);
    });
  })
};

const updateLead = (context, lead) => {
  return new Promise((resolve, reject) => {
    updateLeadInfo(lead).then(res => {
      resolve(res);
    }).catch(e => {
      reject(e);
    });
  })
};

const createLead = (context, lead) => {
  return new Promise((resolve, reject) => {
    createNewLead(lead).then(res => {
      resolve(res);
    }).catch(e => {
      if (e.stack)
        resolve(e.message);

      reject(e);
    });
  })
};

const updateCustomer = (context, customer) => {
  return new Promise((resolve, reject) => {
    updateCustomerInfo(customer).then(res => {
      // context should be called to commit some mutation in order to send popups or something.
      resolve(res);
    }).catch(e => {
      reject(e);
    });
  })
};

const getStatus = (context) => {
  fetchStatus().then(response => {
    context.commit('STATUS_UPDATED', response.data);
  }).catch(err => {
    // ?
  });
};

const getClusters = (context) => {
  fetchClusters().then(response => {
    context.commit('CLUSTERS_UPDATED', response);
  }).catch(err => {
    // ?
  });
};

const getUpdatedClusters = (context) => {
  return new Promise((resolve, reject) => {
    fetchUpdates().then(res => {
      context.commit('UPDATES_UPDATED', res);
    }).catch(e => {
      reject(e);
    });
  })
};

const getClusterProgress = (context, id) => {
  return new Promise((resolve, reject) => {
    fetchClusterProgress(id).then(res => {
      resolve(res);
    }).catch(e => {
      reject(e);
    });
  })
};


const deleteUpdate = (context, id) => {
  return new Promise((resolve, reject) => {
    removeUpdate(id).then(response => {
      resolve(response)
    }).catch(err => {
      console.log(err)
    });
  })
};

const getCustomers = (context) => {
  fetchCustomers().then(response => {
    context.commit('CUSTOMERS_UPDATED', response);
  }).catch(err => {
    // ?
  });
};

const parkingsUnit = (context, unitId) => {
  fetchUnitParking(unitId).then(response => {
    context.commit("UNITS_PARKING_UPDATED", response);
  }).catch(err => {
    console.log(err);
  });
};

const getParking = (context) => {
  fetchParkingLots().then(response => {
    context.commit('PARKING_UPDATED', response);
  }).catch(err => {
    console.log(err);
  });
};

const getQuotingInfo = (context, unitId) => {
  return new Promise((resolve, reject) => {
    fetchQuotingInfo(unitId).then(response => {
      context.commit('QUOTING_UPDATED', response);
      resolve(response);
    }).catch(err => {
      console.log(err);
    });
  })
};

const updateParkingInfo = (context, data) => {
  return new Promise((resolve, reject) => {
    patchParking(data).then(response => {
      resolve(response);
      // context should be called to commit some mutation in order to send popups or something.

    }).catch(e => {
      reject(e);
    });
  })
};

const getLeads = (context) => {
  return new Promise((resolve, reject) => {
    fetchAllLeads().then(res => {
      context.commit('LEADS_UPDATED', res.data);
      // context should be called to commit some mutation in order to send popups or something.
      resolve(res);
    }).catch(e => {
      reject(e);
    });
  })
};

// const getCountUnits = (context) => {
//   fetchCountUnits().then(response => {
//     context.commit('COUNT_DEPARTMENTS_UPDATED', response);
//   }).catch(err => {

//   })
// };


const fetchAllUnits = (context) => {
  return new Promise((resolve, reject) => {
    getUnits().then(res => {
      context.commit('UNITS', res);
      resolve(res);
    }).catch(error => {
      reject(error);
    });
  });
};



const fetchUnitsInfo = (context, clustersId) => {
  return new Promise((resolve, reject) => {
    getUnitsInfo(clustersId).then(res => {
      context.commit('STAGES_INFO', res);
      resolve(res);
    }).catch(error => {
      reject(error);
    })
  })
}

const deleteCustomer = (context, customerId) => {
  return new Promise((resolve, reject) => {
    removeCustomer(customerId).then(response => {
      resolve(response);
    }).catch(error => {
      reject(error);
    });
  });
};

const deleteLead = (context, leadId) => {
  return new Promise((resolve, reject) => {
    removeLead(leadId).then(response => {
      resolve(response);
    }).catch(error => {
      reject(error);
    });
  });
};

const getAWSSignatureUpdate = (context, file) => {
  return new Promise((resolve, reject) => {
    getS3SignatureUpdate(file).then(res => {
      resolve(res);
    }).catch(e => {
      reject(e);
    });
  });
};

export default {
  authenticate,
  getClusters,
  deleteUpdate,
  getUpdatedClusters,
  getCustomers,
  parkingsUnit,
  getParking,
  updateParkingInfo,
  getLeads,
  updateLead,
  deleteCustomer,
  deleteLead,
  getStatus,
  setPlusButton,
  setNewCustomer,
  // getCountUnits,
  fetchAllUnits,
  fetchUnitsInfo,
  updateCustomer,
  createLead,
  getAWSSignatureUpdate,
  getClusterProgress,
  getQuotingInfo
};
