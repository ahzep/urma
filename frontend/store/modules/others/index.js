/* eslint-disable */
import actions from './actions';
import getters from './getters';
import mutations from './mutations';

const state = {
  isAuthenticated: false,
  hasPlusButton: false,
  parking: [],
  parkingsUnit: [],
  leads: [],
  status: [],
  clusters: [],
  countUnits: [],
  customers: [],
  units: [],
  unitsByStage: [],
  unitsInfo: [],
  updates: [],
  quoteInfo: null
};

export default {
  namespaced: true,
  state,
  actions,
  getters,
  mutations,
};
