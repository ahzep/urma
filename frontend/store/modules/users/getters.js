const currentUser = (state) => state.user;
const isAdmin = (state) => state.user.type === 'A' || false;
const users = (state) => state.users;
const executives = state => state.executives;
const agencies = state => state.agencies;
const references = state => state.references;
const commissionsInfo = state => state.commissionsInfo;
const advisers = state => state.advisers;

export default {
  currentUser,
  isAdmin,
  executives,
  users,
  agencies,
  references,
  commissionsInfo,
  advisers
}
