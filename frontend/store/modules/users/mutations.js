const CURRENT_USER_UPDATED = (state, user) => {
  state.user = user;
};
const USERS_UPDATED = (state, users) => {
  state.users = users;
};
const PAGINATION_UPDATED = (state, pagination) => {
  state.pagination = pagination;
};

const USER_UPDATED = (state, user) => {
  for (let i = 0; i < state.users.length; i++) {
    let usr = state.users[i];
    if (usr.id === user.id) {
      for (var key in user) {
        usr[key] = user[key];
      }
    }
  }
};

const EXECUTIVES_UPDATED = (state, executives) => {
  state.executives = executives;
};

const AGENCIES_UPDATED = (state, agents) => {
  state.agencies = agents;
}

const ADVISERS_UPDATED = (state, advisers) => {
  state.advisers = advisers;
}

const REFERENCES_UPDATED = (state, references) => {
  state.references = references;
}

const UNIT_AGENCIES_UPDATED = (state, info) => {
  state.commissionsInfo = info;
}

export default {
  CURRENT_USER_UPDATED,
  PAGINATION_UPDATED,
  USER_UPDATED,
  USERS_UPDATED,
  EXECUTIVES_UPDATED,
  AGENCIES_UPDATED,
  REFERENCES_UPDATED,
  UNIT_AGENCIES_UPDATED,
  ADVISERS_UPDATED
}
