/*VUE*/
import Vue from 'vue';
import Router from 'vue-router';

/* COMPONENTS */
import users from '@/views/users.vue';
// import customers from '@/views/customers.vue';
import dashboardView from '@/views/dashboardView.vue';
import detail from '@/views/detail.vue';
import unitDetail from '@/views/components/unitDetail.vue';
import user from '@/views/user.vue';

/* EXTENSIONS */

Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Vista de dashboard',
      component: dashboardView
    },
    {
      path: '/detail',
      name: 'Listado de desarrollo',
      component: detail
    },
    {
      path: '/admin/users',
      name: 'Usuarios',
      component: users
    },
    {
      path: '/unitDetail',
      name: "Detalle de unidad",
      component: unitDetail
    }
  ]
});
