const local = require('@feathersjs/authentication-local');
const isAdmin = require('../../hooks/is-admin');
const { authenticate } = require('@feathersjs/authentication').hooks;
const { softDelete, iff } = require('feathers-hooks-common');
const isCurrent = adminRole => context => context.params.query.current != undefined && context.params.query.current == 'true';
const accountService = require('../authmanagement/notifier');
const preventSelfDelete = require('../../hooks/prevent-self-delete');
const verifyHooks = require('feathers-authentication-management').hooks;
const togglePagination = require('../../hooks/toggle-pagination');
const client = require('feathers-authentication-management');

const authentication = require('@feathersjs/authentication');
const jwt = require('@feathersjs/authentication-jwt');

const newCustomer = async context => {
  const name = context.data.name;
  const age = context.data.age;
  const address = context.data.address;
  const country = context.data.country;
  const state = context.data.state;
  const city = context.data.city;
  const contactN = context.data.contactNumber;
  const email = context.data.email;
  const userType = '0';

  const customer = {
    name: name,
    age: age,
    address: address,
    country: country,
    state: state,
    city: city,
    userType: userType,
    contactNumber: contactN,
    email: email
  };

  try {
    await context.app.service('/customers').create(customer).then(response => {
      console.log(response);
    });
  } catch (e) {
    console.log(e);
  }
};

const login = async ({ data }) => {

  // If user is trying to log in
  if (data.type === 'login') {
    const strategy = data.strategy;
    const email = data.email;
    const password = data.password;

    return await authenticate(email, password);
  }
};

module.exports = {
  before: {
    all: [
      authenticate('jwt')
    ],
    find: [
      softDelete('deleted'),
      iff(isCurrent(), async context => {
        if (context.params.query.current != undefined && context.params.query.current == 'true') {
          context.result = context.params.user.name === undefined ? await context.app.service('customers').get(context.params.payload.customerId) : context.params.user;
          context.params.user = context.result;
          delete context.params.query.current;
        }
      }),
      togglePagination()
    ],
    get: [softDelete('deleted'), async context => { }],
    create: [
      async context => {
        const newId = await context.app.service('customers').find({ query: { $limit: 1, $sort: { id: -1 } } });
        console.log("before", context.data);
        console.log("newId", newId);
        if (context.data.id <= newId.data[0].id || !context.data.id) context.data.id = newId.data[0].id + 1;
        console.log("after", context.data);
      },

      softDelete('deleted'), isAdmin(), local.hooks.hashPassword(), verifyHooks.addVerification()
    ],
    update: [],
    patch: [],
    remove: [
      isAdmin()
    ]
  },

  after: {
    all: [],
    find: [],
    get: [local.hooks.protect('password'), local.hooks.protect('deleted')],
    create: [local.hooks.protect('password'), local.hooks.protect('deleted')],
    update: [local.hooks.protect('password'), local.hooks.protect('deleted')],
    patch: [local.hooks.protect('password'), local.hooks.protect('deleted')],
    remove: [local.hooks.protect('password'), local.hooks.protect('deleted')]
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
