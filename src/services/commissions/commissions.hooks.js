const { authenticate } = require('@feathersjs/authentication').hooks;
const togglePagination = require('../../hooks/toggle-pagination');
const addAssociations = require('../../hooks/add-associations');
const setClusterObject = require('../../hooks/set-cluster-object');
const setStatusObject = require('../../hooks/set-status-object');
const setExecutiveObject = require('../../hooks/set-executive-object');
const canUpdate = require('../../hooks/can-update');
const preventDuplicate = require('../../hooks/prevent-duplicates');
const iff = require('feathers-hooks-common').iff;

module.exports = {
  before: {
    all: [authenticate('jwt')],
    find: [
      addAssociations({
        models: [
          {
            model:'users',
            as:'user'
          }
        ]
      })
    ],
    get: [],
    create: [
      async context => {
        const newId = await context.app.service('/api/commissions').find({query: {$limit: 1,$sort: {id: -1} }});
        context.data.id = newId.data[0].id + 1;
        console.log(context.data)
      },
    ],
    update: [canUpdate()],
    patch: [],
    remove: [canUpdate()]
  },

  after: {
    all: [
    setStatusObject(),
    setExecutiveObject()

  ],
    find: [

    ],
    get:[

    ],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
