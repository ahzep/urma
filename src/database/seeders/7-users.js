'use strict';

const fs = require('fs');
const csv = require('fast-csv');
const csvFilePath = __dirname + '/../csv/executives.csv'


module.exports = {
  up: (queryInterface, Sequelize) => {
    const bcrypt = require('bcryptjs');
    var salt = bcrypt.genSaltSync(10);
    var x = new Promise(async (resolve, reject) => {
      var seed = [];
      fs.createReadStream(csvFilePath).pipe(csv()).on('data', function (data) {
        seed.push({
          id: data[0],
          username:data[1],
          email:data[2],
          firstName: data[3],
          lastName: data[4],
          telephone: data[5] !== 'null' ? data[5] : null,
          cellphone: data[6] !== 'null' ? data[6] : null,
          password: bcrypt.hashSync(data[7], salt),
          type: data[8],
          age:data[9],
          deleted: data[10],
          createdAt: data[11],
          updatedAt: data[12]
        })
      }).on('end', function (data) {

        resolve(seed);
      })
    }).then(seed => {
      return queryInterface.bulkInsert('users', seed, {});
    });
    return x;
  },


  down: (queryInterface, Sequelize) => {

  }
};
