
'use strict';

const fs = require('fs');
const csv = require('fast-csv');
const csvFilePath = __dirname + '/../csv/units.csv';

module.exports = {
  up: (queryInterface, Sequelize) => {
    var x = new Promise(async (resolve, reject) => {
      var seed = [];
      fs.createReadStream(csvFilePath).pipe(csv()).on('data', function (data) {
        seed.push({
          id: data[0],
          depNumber: data[1] != 'null' ? data[1] : null,
          type: data[2] != 'null' ? data[2] : null,
          bedrooms: data[3] != 'null' ? data[3] : null,
          closets: data[4] != 'null' ? data[4] : null,
          bathrooms: data[5] != 'null' ? data[5] : null,
          parking: data[6] != 'null' ? data[6] : null,
          area1: data[7] != 'null' ? data[7] : null,
          area2: data[8] != 'null' ? data[8] : null,
          area3: data[9] != 'null' ? data[9] : null,
          area4: data[10] != 'null' ? data[10] : null,
          floor: data[11] != 'null' ? data[11] : null,
          aream2: data[12] != 'null' ? data[12] : null,
          aream2total: data[13] != 'null' ? data[13] : null,
          pricem2: data[14] != 'null' ? data[14] : null,
          listPrice: data[15] != 'null' ? data[15] : null,
          cashPrice: data[16] != 'null' ? data[16] : null,
          discount: data[17] != 'null' ? data[17] : null,
          clusterId: data[18] != 'null' ? data[18] : null,
          statusId: data[19] != 'null' ? data[19] : null,
          userId: data[20] != 'null' ? data[20] : null,
          createdAt: new Date(),
          updatedAt: new Date()
        });
      }).on('end', function (data) {

        resolve(seed);
      });
    }).then(seed => {
      return queryInterface.bulkInsert('units', seed, {});
    });
    return x;
  },

  down: (queryInterface, Sequelize) => {

  }
};
