'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('clusters', [
      {
        name: 'MAR AMARILLO 1471',
        code: 1,
        abb: 'MAR',
        createdAt: new Date(),
        updatedAt: new Date()
      }
    ], {});
  },

  down: (queryInterface, Sequelize) => {
  }
};
