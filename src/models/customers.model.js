'use strict';
const Sequelize = require('sequelize');
const DataTypes = Sequelize.DataTypes;

module.exports = (app) => {
  const sequelizeClient = app.get('sequelizeClient');
  const customers = sequelizeClient.define('customers', {
    name: { type: DataTypes.STRING, allowNull: true },
    lastName: { type: DataTypes.STRING, allowNull: true },
    age: { type: DataTypes.INTEGER, allowNull: false },
    address: { type: DataTypes.STRING },
    zipcode: { type: DataTypes.INTEGER },
    contactNumber: { type: DataTypes.STRING, allowNull: true },
    cellphone: { type: DataTypes.STRING, allowNull: true },
    email: { type: DataTypes.STRING, allowNull: false, unique: true },
    country: { type: DataTypes.STRING },
    state: { type: DataTypes.STRING },
    city: { type: DataTypes.STRING },
    password: { type: DataTypes.STRING, allowNull: true },
    userType: { type: DataTypes.STRING, allowNull: false },
    occupation: { type: DataTypes.STRING, allowNull: true },
    deleted: { type: DataTypes.BOOLEAN, defaultValue: false },
    entity: { type: DataTypes.INTEGER, allowNull: false },
    codeId: { type: DataTypes.STRING, allowNull: true },
    coOwner: { type: DataTypes.STRING, allowNull: true },
    representative: { type: DataTypes.STRING, allowNull: true },
    dob: { type: DataTypes.DATE, allowNull: true },
    civilStatus: { type: DataTypes.STRING, allowNull: true }
  }, {});
  customers.associate = function (models) {

  };
  return customers;
};
