'use strict';
const Sequelize = require('sequelize');
const DataTypes = Sequelize.DataTypes;

module.exports = (app) => {
  const sequelizeClient = app.get('sequelizeClient');
  const units = sequelizeClient.define('units', {
    id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true },
    depNumber: { type: DataTypes.STRING },
    type: { type: DataTypes.STRING },
    bedrooms: { type: DataTypes.INTEGER },
    closets: { type: DataTypes.INTEGER },
    bathrooms: { type: DataTypes.DOUBLE },
    parking: { type: DataTypes.INTEGER },
    area1: { type: DataTypes.STRING },
    area2: { type: DataTypes.STRING },
    area3: { type: DataTypes.STRING },
    area4: { type: DataTypes.STRING },
    floor: { type: DataTypes.INTEGER },
    aream2: { type: DataTypes.STRING },
    aream2total: { type: DataTypes.DOUBLE },
    pricem2: { type: DataTypes.INTEGER },
    listPrice: { type: DataTypes.DOUBLE },
    cashPrice: { type: DataTypes.DOUBLE },
    discount: { type: DataTypes.DOUBLE },
  }, {});

  units.associate = function (models) {
    units.belongsTo(models.clusters);
    units.belongsTo(models.status);
    units.belongsTo(models.users);
  };

  return units;
};
