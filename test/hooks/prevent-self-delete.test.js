const assert = require('assert');
const feathers = require('@feathersjs/feathers');
const preventSelfDelete = require('../../src/hooks/prevent-self-delete');

describe('\'preventSelfDelete\' hook', () => {
  let app;

  beforeEach(() => {
    app = feathers();

    app.use('/dummy', {
      async get(id) {
        return { id };
      }
    });

    app.service('dummy').hooks({
      before: preventSelfDelete()
    });
  });

  it('runs the hook', async () => {
    const result = await app.service('dummy').get('test');
    
    assert.deepEqual(result, { id: 'test' });
  });
});
